import sample.DataSource

plugins {
    id 'java'
    id 'com.bmuschko.cargo' version '2.6.1'
}

repositories {
    maven { url "http://nexus.onebusaway.org/content/groups/public" }
    mavenCentral()
    jcenter()
}

configurations {
    tomcat
    tomcatDeps
    warApp { transitive = false }
}

def slf4j_version = '1.7.26'
def oba_version = '1.1.19'
def tomcat_version = '9.0.20'

sourceCompatibility = JavaVersion.VERSION_1_8
targetCompatibility = JavaVersion.VERSION_1_8

// https://docs.gradle.org/current/userguide/managing_transitive_dependencies.html#sec:dependency_constraints
dependencies {
    tomcat "org.apache.tomcat:tomcat:$tomcat_version@zip"
    tomcatDeps "org.apache.tomcat:tomcat-juli:$tomcat_version",
            "org.apache.tomcat.extras:tomcat-extras-juli-adapters:9.0.0.M6"

    warApp "org.onebusaway:onebusaway-api-webapp:$oba_version@war",
            "org.onebusaway:onebusaway-transit-data-federation-webapp:$oba_version@war"

    runtimeClasspath ("org.onebusaway:onebusaway-transit-data-federation-builder:$oba_version") {
        exclude group: 'org.slf4j'
    }
    runtimeClasspath 'org.postgresql:postgresql:42.2.5'
    // Manually add slf4j because the dependencies did not have matching versions
    runtimeClasspath "org.slf4j:slf4j-log4j12:$slf4j_version"
    runtimeClasspath "org.slf4j:slf4j-api:$slf4j_version"
}

// copy bundle resources to the expected directory
task bundleRes(type: Copy) {
    from("res") {
        exclude '*.xml'
    }
    into "$buildDir/res/"
}

// https://github.com/OneBusAway/onebusaway-application-modules/wiki/Transit-Data-Bundle-Guide
task bundleTransit(type: JavaExec, dependsOn: bundleRes, group: 'build') {
    description = 'Build the transit data bundle'
    outputs.dir("$bundle_dir")
    inputs.files(bundleRes.outputs)

    classpath = configurations.runtimeClasspath
    maxHeapSize = '1g'

    args "$buildDir/res/gtfs.zip", "$bundle_dir"
    jvmArgs "-server"

    main = 'org.onebusaway.transit_data_federation.bundle.FederatedTransitDataBundleCreatorMain'
}

task apiWebAppDs(type: DataSource, group: 'build') {
    archiveFileName = provider { 'onebusaway-api-webapp.war' }
    destinationDirectory = file(webapp_dir)

    dataSourceXml {
        source = file('res/data-sources-web-api.xml')
        expansions = [
                bundlePath: bundleTransit.outputs.files.singleFile.path,
                *: project.ext.properties
        ]
    }
}

task apiFedWebAppDs(type: DataSource, group: 'build') {
    archiveFileName = provider { 'onebusaway-transit-data-federation-webapp.war' }
    destinationDirectory = file(webapp_dir)

    dataSourceXml {
        source = file('res/data-sources-web-fed.xml')
        expansions = [bundlePath: bundleTransit.outputs.files.singleFile.path]
    }
}

cargo {
    containerId = 'tomcat9x'
    port = Integer.parseInt(cargo_port)

    deployable {
        file = file("$webapp_dir/onebusaway-api-webapp.war")
        context = 'oba-api'
    }

    deployable {
        file = file("$webapp_dir/onebusaway-transit-data-federation-webapp.war")
        context = 'oba-fed'
    }

    local {
        logLevel = 'high'
        homeDir = file('build/tmp/catalina/')
        extraClasspath = configurations.tomcatDeps
        sharedClasspath = configurations.runtimeClasspath
        containerProperties {
            property 'cargo.tomcat.webappsDirectory', 'build/webapps'
            property 'cargo.tomcat.ajp.port', 9099
        }
        configFile {
            files = files('res/log4j.properties')
            toDir = 'lib'
        }
        installer {
            installConfiguration = configurations.tomcat
            downloadDir = file('build/tomcat/')
            extractDir = file('build/tomcat/')
        }
    }
}

wrapper {
    distributionType Wrapper.DistributionType.ALL
    gradleVersion '5.6'
}

[apiFedWebAppDs, apiWebAppDs]*.dependsOn bundleTransit
[cargoConfigureLocal, cargoRunLocal, cargoStartLocal]*.dependsOn apiFedWebAppDs, apiWebAppDs
