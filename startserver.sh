#!/bin/bash

function get_id() {
    python -c "$(cat <<-'EOF'

	import sys
	import json

	for loc in json.load(sys.stdin)["results"]["locations"]:
	  if loc["n"].lower() == "saskatoon":
	    print (loc["id"])
	    sys.exit(0)

EOF
)"
}

function get_gtfs() {
    python -c "$(cat <<-'EOF'

	import sys
	import json
	print (json.load(sys.stdin)["results"]["feeds"][0]['u'].get('d', 'http://apps2.saskatoon.ca/app/data/google_transit.zip'))

EOF
)"
}

if ! [ -f 'res/gtfs.zip' ]; then
  #Transit data API
  api_key='ba141a22-02bf-4659-89c1-f01f2bf79bde'
  api_url='https://api.transitfeeds.com/v1'

  saskatoon_id="$(curl -X GET --header 'Accept: application/json' "$api_url/getLocations?key=$api_key" | get_id)"
  saskatoon_gtfs_zip="$(curl -X GET --header 'Accept: application/json' "$api_url/getFeeds?key=$api_key&location=$saskatoon_id&descendants=0&page=1&limit=1&type=gtfs" | get_gtfs)"

  curl -X GET "$saskatoon_gtfs_zip" -o res/gtfs.zip
fi

echo 'Remember to start the database, this is usually done from systemd'
echo 'Enjoy!'
echo

JAVA_HOME="${JAVA_HOME:-/usr/lib/jvm/java-8-openjdk/}"
./gradlew cargoRunLocal -Dorg.gradle.java.home="$JAVA_HOME"
