package sample

class DataSourceXmlConfig {
    /**
     * This is the source we want to add to the configuration
     */
    File source

    /**
     * This is a map of extra properties you want to inject into
     * the above {@link #source}.
     *
     * This is usually done using the [expand]{@link org.gradle.api.tasks.AbstractCopyTask#expand(java.util.Map)}
     */
    Map<String, ?> expansions = [:]
}
