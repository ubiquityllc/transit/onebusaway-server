package sample

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import org.gradle.api.artifacts.Dependency
import org.gradle.api.file.CopySpec
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.bundling.Zip
import org.gradle.util.ConfigureUtil

/**
 * The basic purpose of DataSource class is to provide our own custom configurations for
 * the onebusaway webapp and api's
 *
 * @see {@link #dataSourceXml}
 *
 * We use this as an alternative to having to create our own war and compiling
 * spring and stuff
 */
@TypeChecked
class DataSource extends Zip {
    @Internal
    final Property<File> warFile = project.objects.property(File.class)

    DataSource () {
        warFile.convention(project.provider {
            def files = project.configurations.getByName('warApp').fileCollection { Dependency d ->
                d.name == (archiveFileName.getOrElse("") - (~/\.[^.]+$/))
            }
            files.empty ? null : files.first()
        })
    }

    /**
     * This function is used to specify the location of data-sources.xml configuration
     * and injects it into the archive by replacing any existing data-source.xml with the one
     * we choose
     *
     * @see {@link DataSourceXmlConfig#source}
     *
     * @param dsConf The configuration object used to specify the location of the
     * file as well as any extra variables which should be injected into the file
     */
    @CompileStatic
    void dataSourceXml (@DelegatesTo(value = DataSourceXmlConfig, strategy = Closure.DELEGATE_FIRST) Closure dsConf) {
        filesToUpdate project.copySpec {
            DataSourceXmlConfig ds = ConfigureUtil.configure(dsConf, new DataSourceXmlConfig())

            exclude('**/WEB-INF/classes/data-sources.xml')
            from(ds.source) {
                if (ds.expansions) {
                    expand(ds.expansions)
                }
                into('WEB-INF/classes/')
                rename { 'data-sources.xml' }
            }
        }
    }

    @CompileStatic
    private def filesToUpdate (CopySpec spec) {
        if (warFile.isPresent()) {
            from(project.zipTree(warFile))
            with spec
        }
    }
}
